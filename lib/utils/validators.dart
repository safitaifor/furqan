
extension StringValidator on String {
  bool isValidPassword() {
    if(length > 5){
      return true;
    }else{
      return false;
    }
  }
  bool isValidName() {
    if(length > 2){
      return true;
    }else{
      return false;
    }
  }
  bool isValidPhone() {
    if(length == 12 && substring(0,3)=='966'){
      return true;
    }else{
      return false;
    }
  }
  bool isValidEmail() {
    return RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
  bool isValidID() {
    if( length == 11 ){
      return true;
    }else{
      return false;
    }
  }
}

extension AgeValidator on DateTime{
  bool isValidAge() {
    if( DateTime.now().year - year > 17 ){
      return true;
    }else{
      return false;
    }
  }
}
