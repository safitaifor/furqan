import 'package:flutter/material.dart';
import 'package:furqan_gallery/model/product.dart';

class ProductItem extends StatelessWidget {
  final Function onTapped;
  final Product product;
  final int index;

  const ProductItem({Key? key, required this.onTapped, required this.product, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>onTapped(),
      child: Container(
        height: 130,
        margin:const EdgeInsets.all(8),
        padding:const EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.lightBlueAccent.withOpacity(index.isOdd? 0.5:0.3),
          borderRadius: BorderRadius.circular(4),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Text(
              product.name,
              style: TextStyle(
                fontSize: 20,
                color: Colors.black.withOpacity(0.8),
                fontWeight: FontWeight.bold,
                fontFamily: 'Tajawal',
              ),
            ),
            const SizedBox(height: 8,),
            Text(
              "الرصيد: ${product.balance}",
              style: TextStyle(
                fontSize: 16,
                color: Colors.black.withOpacity(0.8),
                fontWeight: FontWeight.bold,
                fontFamily: 'Tajawal',
              ),
            ),

            const SizedBox(height: 16),
            Divider(
              color: Colors.black.withOpacity(0.2),
              height: 1,
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '\$${product.sellingPrice}',
                  style: const TextStyle(
                    fontSize: 22,
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
