import 'package:flutter/material.dart';

class BorderWithTitle extends StatelessWidget {
  final Widget child;
  final String title;
  final EdgeInsets? padding;
  const BorderWithTitle({Key? key, required this.child, required this.title, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0 ,bottom: 8 ,left: 12,right: 12),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Column(
            children: [
              DecoratedBox(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.brown,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: padding??const EdgeInsets.only(left: 8,right: 8,bottom: 16,top: 16),
                  child: child,
                ),
              ),
            ],
          ),
          Positioned(
            top: -10,
            right: 10,
            child: Container(
              padding:const EdgeInsets.symmetric(horizontal: 4),
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Text(
                title,
                style:const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.brown,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
