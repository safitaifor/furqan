import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<bool?> showToast(
    {
      required String text,
      Color color = Colors.green,
      Color textColor = Colors.white,
      ToastGravity toastGravity = ToastGravity.SNACKBAR,
    }) {
  return Fluttertoast.showToast(
    msg: text,
    toastLength: Toast.LENGTH_SHORT,
    gravity: toastGravity,
    timeInSecForIosWeb: 1,
    backgroundColor: color,
    textColor: textColor ,
    fontSize: 14.0,
  );
}

Future<bool?> showErrorToast(
    {
      required dynamic error,
      Color color = Colors.blueAccent,
      Color textColor = Colors.white,
      ToastGravity toastGravity = ToastGravity.SNACKBAR
    }) {
  return Fluttertoast.showToast(
    msg: '$error',
    toastLength: Toast.LENGTH_LONG,
    gravity: toastGravity,
    timeInSecForIosWeb: 1,
    backgroundColor: color,
    textColor: textColor,
    fontSize: 14.0,
  );
}
