import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:furqan_gallery/model/thousands_formatter.dart';


class QuantityInputWidget extends StatelessWidget {
  final double maxQuantity;
  final TextEditingController textEditingController;
  final  String title;
  final bool readOnly ;
  final Function? onChange;
  final Function validator;
  final String? error;
  final bool isIntValue;

  const QuantityInputWidget({
    Key? key,
    this.maxQuantity = double.infinity,
    required this.title,
    required this.textEditingController,
    this.onChange,
    this.readOnly = false,
    required this.validator,
    this.error,
    this.isIntValue = false,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    double quantity = double.tryParse(textEditingController.text)??0;


    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0 ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: TextFormField(
              controller: textEditingController,
              keyboardType: TextInputType.number,
              textAlignVertical: TextAlignVertical.bottom,
              readOnly: readOnly,
              style: const TextStyle(fontSize: 18),
              textAlign: TextAlign.start,
              inputFormatters: [
                TextInputFormatter.withFunction((oldValue, newValue) {
                  // يتم إزالة أي شيء غير رقمي أو علامة فاصلة عند العودة من newValue
                  final cleanText = newValue.text.replaceAll(isIntValue?RegExp('[^0-9]') :RegExp('[^0-9.]'), '');

                  // يتم التأكد من أن العدد لا يحتوي على أكثر من علامة فاصلة واحدة
                  final parts = cleanText.split('.');
                  if (parts.length > 2) {
                    return oldValue;
                  }
                  if (parts.length == 2 && parts.last.length > 2) {
                    return oldValue;
                  }

                  return newValue.copyWith(text: cleanText);
                }),
              ],
              decoration: InputDecoration(
                labelText: title,
                errorText: error,
                border:const OutlineInputBorder(),
              ),
              validator: (value){

                return validator(value);
              },
              onChanged: (value) {
                if(onChange!=null){
                  onChange!(value);
                }
              },
            ),
          ),
          Column(
            children: [
              InkWell(
                onTap: quantity < maxQuantity && !readOnly
                    ? (){
                  if(textEditingController.text.isEmpty){
                    textEditingController.text = "1";
                    quantity = 1;
                  }else{
                    double? oldQuantity =double.tryParse(textEditingController.text);
                    if(oldQuantity == null){
                      //error = "قيمة غير صالحة";
                    }else if(oldQuantity < 0){
                      quantity = 1;
                      textEditingController.text = quantity.toString();
                    }else{
                      quantity = oldQuantity+1;
                      textEditingController.text = quantity.toString();
                    }
                  }
                  if(onChange!=null) {
                    onChange!(textEditingController.text);
                  }
                }: null,
                child: SizedBox(
                  height: 30,
                  child:
                  Icon(Icons.arrow_drop_up,
                      color: quantity < maxQuantity && !readOnly? Colors.black : Colors.grey),

                ),
              ),
              InkWell(
                onTap: quantity > 0 && !readOnly && textEditingController.text.isNotEmpty
                    ? () {
                  double? oldQuantity =double.tryParse(textEditingController.text);
                  if(oldQuantity == null){

                  }else if(oldQuantity > maxQuantity){
                    quantity = maxQuantity;
                    textEditingController.text = quantity.toString();
                  }else{
                    quantity = oldQuantity-1;
                    textEditingController.text = quantity.toString();
                  }

                  if(onChange!=null) {
                    onChange!(textEditingController.text);
                  }
                }: null,
                child: SizedBox(
                  height: 30,
                  child: Icon(
                    Icons.arrow_drop_down,
                    color: quantity > 0 && !readOnly && textEditingController.text.isNotEmpty? Colors.black : Colors.grey,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

