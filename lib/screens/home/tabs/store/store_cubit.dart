import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:furqan_gallery/local/product_operations.dart';
import 'package:furqan_gallery/model/product.dart';

part 'store_state.dart';

class StoreCubit extends Cubit<StoreState> {
  StoreCubit() : super(StoreInitial());

  static StoreCubit get(context) => BlocProvider.of(context);

  List<Product> products = [];

  init()async{
    emit(StoreLoading());
    ProductOperations  productOperations = ProductOperations();
    products = await productOperations.getAllProducts();
    emit(StoreDone());
  }



}
