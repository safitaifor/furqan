part of 'store_cubit.dart';

abstract class StoreState {}

class StoreInitial extends StoreState {}

class StoreLoading extends StoreState {}

class StoreError extends StoreState {}

class StoreDone extends StoreState {}

class StoreUpdate extends StoreState {}
