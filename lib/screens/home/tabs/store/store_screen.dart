import 'package:furqan_gallery/model/product.dart';
import 'package:furqan_gallery/screens/home/add_product/product_form_screen.dart';
import 'package:furqan_gallery/widgets/items/product_widget.dart';

import 'store_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StoreScreen extends StatelessWidget {
  const StoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<StoreCubit, StoreState>(
      builder: (context, state) {
        if(state is StoreLoading){
          return const Center(child: CircularProgressIndicator());
        }
        if(StoreCubit.get(context).products.isEmpty){
          return const Center(child: Text('المخزن فارغ'));
        }


        return ListView.builder(
          itemCount: StoreCubit.get(context).products.length,
          itemBuilder: (context , index){
            Product product = StoreCubit.get(context).products[index];
            return ProductItem(
              product: product,
              index: index,
              onTapped: (){
                Navigator.push(context, MaterialPageRoute(builder: (context1) => ProductFormScreen(product: product,storeCubit: StoreCubit.get(context),)));
              },
            );
          },
        );
      },
    );
  }
}
