part of 'invoices_cubit.dart';

abstract class InvoicesState {}

class InvoicesInitial extends InvoicesState {}

class InvoicesLoading extends InvoicesState {}

class InvoicesError extends InvoicesState {}

class InvoicesDone extends InvoicesState {}

class InvoicesUpdate extends InvoicesState {}
