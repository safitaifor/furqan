import 'package:furqan_gallery/model/invoice.dart';
import 'package:furqan_gallery/screens/pdf/pdf_viewer.dart';

import 'invoices_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';





class InvoicesScreen extends StatelessWidget {
  const InvoicesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<InvoicesCubit, InvoicesState>(
      builder: (context, state) {
        if(state is InvoicesLoading){
          return const Center(child: CircularProgressIndicator());
        }
        if(InvoicesCubit.get(context).invoices.isEmpty){
          return const Center(child: Text("لا يوجد فواتير متاحة"),);
        }
        return ListView.builder(
          itemCount: InvoicesCubit.get(context).invoices.length,
          itemBuilder: (context , index){
            Invoice invoice = InvoicesCubit.get(context).invoices[index];

            return InkWell(
              onTap: (){
                InvoicesCubit.get(context).savePdf(context, invoice);
              },
              child: Container(
                height: 130,
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                decoration: BoxDecoration(
                color: Colors.lightBlueAccent.withOpacity(index.isOdd? 0.5:0.3),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("صاحب العلاقة: " , style:TextStyle(fontSize: 16 , fontWeight: FontWeight.bold),),
                        Text(invoice.customerId.name , style:const TextStyle(fontSize: 16 ),),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("التاريخ: " , style:TextStyle(fontSize: 16 , fontWeight: FontWeight.bold),),
                        Text(invoice.invoiceDate.substring(0,10) , style:const TextStyle(fontSize: 16 ),),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("عدد الاقساط: " , style:TextStyle(fontSize: 16 , fontWeight: FontWeight.bold),),
                        Text(invoice.paymentCount.toInt().toString() , style:const TextStyle(fontSize: 16 ),),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("المادة: " , style:TextStyle(fontSize: 16 , fontWeight: FontWeight.bold),),
                        Text(invoice.productId.name , style:const TextStyle(fontSize: 16 ),),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("المبلغ الاجامالي: " , style:TextStyle(fontSize: 16 , fontWeight: FontWeight.bold),),
                        Text('${(invoice.productId.sellingPrice*invoice.quantity).toStringAsFixed(2)}\$' , style:const TextStyle(fontSize: 16 ),),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
