import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:furqan_gallery/local/invoice_operations.dart';
import 'package:furqan_gallery/model/invoice.dart';
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:furqan_gallery/screens/pdf/pdf_viewer.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;


part 'invoices_state.dart';

class InvoicesCubit extends Cubit<InvoicesState> {
  InvoicesCubit() : super(InvoicesInitial());

  static InvoicesCubit get(context) => BlocProvider.of(context);

  List<Invoice> invoices = [];
  final InvoicesOperations _invoicesOperations = InvoicesOperations();

  init()async{
    emit(InvoicesLoading());
    invoices =await _invoicesOperations.getAllInvoices();

    emit(InvoicesDone());
  }

  String? pdfPath;

  Future<String> getFilePath(String fileName) async {
    final directory = await getApplicationDocumentsDirectory();
    return '${directory.path}/$fileName';
  }
  pw.MemoryImage getImage(String imagePath){
    final bytes = File(imagePath).readAsBytesSync();
    final imageProvider = pw.MemoryImage(bytes);
    return imageProvider;
  }

  pw.Widget timeFieldView ({required pw.Font ttf , required String title  ,required String value}){

    return pw.Row(
      children: [
        pw.Container(
          padding:const pw.EdgeInsets.all(8),
          width: 75,
          height: 28,
          color: PdfColors.grey200,
          alignment: pw.Alignment.center,
          child: pw.Text(value,
              style: pw.TextStyle(font: ttf,fontSize: 14 )),
        ),
        pw.Container(
          padding:const pw.EdgeInsets.all(8),
          width: 50,
          height: 28,
          color: PdfColors.blue,
          alignment: pw.Alignment.center,
          child: pw.Text(title,
              style: pw.TextStyle(font: ttf,fontSize: 14  ,color: PdfColors.white)),
        ),
      ],
    );
  }

  pw.Widget fieldView ({required pw.Font ttf , required String title  ,required String value , required double width }){

    return pw.Row(
      children: [
        pw.Row(
          children: [
            pw.Container(
              padding:const pw.EdgeInsets.all(8),
              width: width,
              height: 40,
              color: PdfColors.grey200,
              alignment: pw.Alignment.center,
              child: pw.Text(value,
                  style: pw.TextStyle(font: ttf,fontSize: 12 )),
            ),
            pw.Container(
              padding:const pw.EdgeInsets.all(8),
              width: 80,
              height: 40,
              color: PdfColors.blue,
              alignment: pw.Alignment.center,
              child: pw.Text(title,
                  style: pw.TextStyle(font: ttf,fontSize: 12  ,color: PdfColors.white)),
            ),
          ],
        ),
      ],
    );
  }


  void savePdf(BuildContext context , Invoice selectedInvoice) async {
    final pdf = pw.Document();
    final ByteData image = await rootBundle.load('assets/images/mastercard-image.jpg');
    Uint8List imageData = (image).buffer.asUint8List();
    var font = await rootBundle.load("assets/fonts/Tajawal-Medium.ttf");
    final ttf = pw.Font.ttf(font);
    font = await rootBundle.load("assets/fonts/Tajawal-ExtraBold.ttf");
    final ttfBold = pw.Font.ttf(font);


    final tableHeaders = ['رقم الدفعة', 'التاريخ', 'مبلغ الدفعة', 'الحالة'];
    final tableData = List.generate(
      selectedInvoice.paymentCount,
      (index) => [
        "الدفعة ${index + 1}",
        DateTime.parse(selectedInvoice.firstInstallmentDate)
            .add(Duration(days: 30 * index)),
        (index <selectedInvoice.paymentCount-1)?selectedInvoice.installmentValue:selectedInvoice.lastInstallmentValue,
        "لم يتم التسديد"
      ],
    );

    pdf.addPage(
      pw.Page(
          build: (context) => pw.Directionality(
            textDirection: pw.TextDirection.rtl,
            child: pw.Padding(
              padding:const pw.EdgeInsets.all(28),
              child: pw.Column(
                mainAxisAlignment: pw.MainAxisAlignment.start,
                children: [
                  // العنوان
                  pw.Row(
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: [
                        pw.Column(
                          children: [
                            pw.Image(pw.MemoryImage(imageData),width: 130),
                            pw.Text('0994359113',style: pw.TextStyle(font: ttfBold,fontSize: 16 ,fontWeight: pw.FontWeight.bold )),
                          ],
                        ),
                        pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.end,
                          children: [
                            pw.SizedBox(height: 34),
                            pw.Text('معرض الفرقان للتقسيط',
                                style: pw.TextStyle(font: ttfBold, fontSize: 16 ,fontWeight: pw.FontWeight.bold )),
                            pw.SizedBox(height: 4),
                            pw.Text('     لبيع كافة الاجهزة الكهربائية بالتقسيط المريح',
                                style: pw.TextStyle(font: ttf,fontSize: 16 , )),
                            pw.SizedBox(height: 4),
                            pw.Text('النجف الاشرف-مقابل مستشفى الحكيم',
                                style: pw.TextStyle(font: ttf,fontSize: 16 , )),
                          ],
                        ),

                      ]
                  ),
                  pw.SizedBox(height: 32),

                  //تفاصيل الفاتورة
                  pw.Column(
                    mainAxisAlignment: pw.MainAxisAlignment.start,
                    children: [
                      timeFieldView(ttf: ttf, title: "الوقت", value: "${DateTime.now().hour}:${DateTime.now().minute} ${DateTime.now()}"),
                      pw.SizedBox(height: 4),
                      pw.Row(
                        children: [
                          fieldView(ttf: ttf, title: "الكمية", value: selectedInvoice.quantity.toString(), width: 150),
                          pw.SizedBox(width: 4),
                          fieldView(ttf: ttf, title: "اسم المادة", value: selectedInvoice.productId.name, width: 150),
                        ],
                      ),
                      pw.SizedBox(height: 4),
                      pw.Row(
                        children: [
                          fieldView(ttf: ttf, title: "تاريخ القائمة", value: selectedInvoice.invoiceDate, width: 150),
                          pw.SizedBox(width: 4),
                          fieldView(ttf: ttf, title: "رقم القائمة", value: selectedInvoice.id.toString(), width: 150),
                        ],
                      ),
                      pw.SizedBox(height: 4),
                      pw.Row(
                        children: [
                          fieldView(ttf: ttf, title: "رقم الهاتف", value: selectedInvoice.customerId.phoneNumber, width: 150),
                          pw.SizedBox(width: 4),
                          fieldView(ttf: ttf, title: "اسم الزبون", value: selectedInvoice.customerId.name, width: 150),
                        ],
                      ),
                      pw.SizedBox(height: 4),
                      pw.Row(
                        children: [
                          fieldView(ttf: ttf, title: "العنوان", value: selectedInvoice.customerId.address, width: 150),
                          pw.SizedBox(width: 4),
                          fieldView(ttf: ttf, title: "مبلغ القائمة", value: (selectedInvoice.productId.sellingPrice*selectedInvoice.quantity).toString(), width: 150),
                        ],
                      ),
                      pw.SizedBox(height: 4),
                      pw.Row(
                        children: [
                          fieldView(ttf: ttf, title: "اسم التاجر", value: selectedInvoice.merchantId!.name, width: 150),
                          pw.SizedBox(width: 4),
                          fieldView(ttf: ttf, title: "الوظيفة", value: selectedInvoice.customerId.job, width: 150),
                        ],
                      ),
                    ],
                  ),

                  pw.SizedBox(height: 16),
                  //جدول الدفعات
                  pw.Table.fromTextArray(
                    headers: tableHeaders,
                    data: tableData,
                    headerStyle: pw.TextStyle(
                      fontWeight: pw.FontWeight.bold,
                      color: PdfColors.white,
                      font: ttf,
                    ),
                    cellStyle: pw.TextStyle(
                      color: PdfColors.black,
                      font: ttf,
                    ),
                    cellAlignments: {
                      0: pw.Alignment.centerRight,
                      1: pw.Alignment.centerRight,
                      2: pw.Alignment.centerRight,
                      3: pw.Alignment.centerRight,
                    },
                    headerCellDecoration: const pw.BoxDecoration(
                        color: PdfColors.blue
                    ),

                    border: pw.TableBorder.all(width: 2),
                  ),
                ],
              ),
            ),
          )
      ),
    );

    final filePath = await getFilePath('example.pdf');
    File(filePath).writeAsBytesSync(await pdf.save());
    pdfPath = filePath;
    if(pdfPath!= null) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => PdfViewScreen(pdfPath: pdfPath!)));
    }
      emit(InvoicesDone());
  }




}
