
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:furqan_gallery/local/customer_operations.dart';
import 'package:furqan_gallery/local/document_operations.dart';
import 'package:furqan_gallery/local/invoice_operations.dart';
import 'package:furqan_gallery/local/person_operations.dart';
import 'package:furqan_gallery/local/product_operations.dart';
import 'package:furqan_gallery/model/customer.dart';
import 'package:furqan_gallery/model/document.dart';
import 'package:furqan_gallery/model/invoice.dart';
import 'package:furqan_gallery/model/person.dart';
import 'package:furqan_gallery/model/product.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:intl/intl.dart';

part 'add_invoice_form_state.dart';

class AddInvoiceFormCubit extends Cubit<AddInvoiceFormState> {
  AddInvoiceFormCubit() : super(AddInvoiceFormInitial());

  static AddInvoiceFormCubit get(context) => BlocProvider.of(context);
  late GlobalKey<FormState> formKey;
  bool readOnly = false;
  bool isUSD = false;
  List<Customer> customers=[];
  List<Person> promoter =[] ;
  List<Person> merchants =[];
  List<Product> products = [];
  List<Document> documents = [];
  List<Document> selectedDocuments = [];



  ProductOperations productOperations = ProductOperations();
  CustomerOperations customerOperations = CustomerOperations();
  PersonOperations personOperations = PersonOperations();
  DocumentOperations documentOperations = DocumentOperations();


  // customer info
  final TextEditingController customerNameController =TextEditingController();
  final TextEditingController phoneNumberController =TextEditingController();
  late final TextEditingController jobController=TextEditingController();
  late final TextEditingController codeController=TextEditingController();
  late final TextEditingController addressController=TextEditingController();
  late final TextEditingController cardTypeController=TextEditingController();

  // product info
  late final TextEditingController productNameController=TextEditingController();
  late final TextEditingController purchasePriceController=TextEditingController();
  late final TextEditingController sellingPriceController=TextEditingController();
  late final TextEditingController balanceController=TextEditingController();
  late final TextEditingController quantityController=TextEditingController();

  // Invoices info
  late final TextEditingController paymentDateController=TextEditingController();
  late final TextEditingController firstPaymentDateController=TextEditingController();
  late final TextEditingController paymentsCountController=TextEditingController();
  late final TextEditingController firstPaymentController=TextEditingController();
  late final TextEditingController remainingAmountController=TextEditingController();
  late final TextEditingController installmentValueController=TextEditingController();
  late final TextEditingController lastInstallmentValueController=TextEditingController();

  late DateTime paymentDate;
  late DateTime firstPaymentDate;



  Customer? selectedCustomer ;
  Product? selectedProduct;
  Person? selectedPromoter;
  Person? selectedMerchant;
  late SaleType saleType = SaleType.installment;
  String? errorTextQuantity ;
  double maxQuantity = 0;

  init()async{
    formKey = GlobalKey<FormState>();
    products = await productOperations.getAllProducts().then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    customers = await customerOperations.getAllCustomers().then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });

    promoter  = await personOperations.getAllPersons(PersonType.promoter).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    merchants = await personOperations.getAllPersons(PersonType.merchant).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    documents = await documentOperations.getAllDocuments().then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });


    // dates init
    paymentDate = DateTime.now();
    firstPaymentDate = DateTime.now().add(const Duration(days: 30));
    paymentDateController.text = DateFormat('yyyy-MM-dd').format(paymentDate);
    firstPaymentDateController.text = DateFormat('yyyy-MM-dd').format(firstPaymentDate);

  }

  update()async{
    products = await productOperations.getAllProducts().then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    customers = await customerOperations.getAllCustomers().then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });

    selectedPromoter = null;
    selectedMerchant= null;
    promoter  = await personOperations.getAllPersons(PersonType.promoter).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    merchants = await personOperations.getAllPersons(PersonType.merchant).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
    emit(AddInvoiceFormUpdate());
  }
  updatePromotersOnly()async{
    selectedPromoter = null;
    promoter  = await personOperations.getAllPersons(PersonType.promoter).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
  }
  updateMerchantsOnly()async{
    selectedMerchant = null;
    merchants  = await personOperations.getAllPersons(PersonType.merchant).then((value) {
      emit(AddInvoiceFormUpdate());
      return value;
    });
  }

  Future<void> addInvoice() async {
    if(selectedProduct == null){
      showToast(text: "الرجاء اختيار المادة اولاً ", color: Colors.redAccent);
      return;
    }

    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      late int customerId;
      if(selectedCustomer == null){
        customerId = await customerOperations.insertCustomer(Customer(
            name: customerNameController.text,
            phoneNumber: phoneNumberController.text,
            job: jobController.text,
            code: codeController.text,
            address: addressController.text,
            cardType: cardTypeController.text));
        // في حال فشلت إضافة الزبون
        if(customerId == -1){
          showToast(text: "عذرا فشلت العملية يرجى مراجعة تقاصيل الزبون", color: Colors.redAccent);
          return;
        }

      }else{
        customerId = selectedCustomer!.id!;
      }

      // create invoice object
      try {
        Invoice invoice = Invoice(
          customerId:selectedCustomer ?? Customer(
            id: customerId,
              name: customerNameController.text,
              phoneNumber: phoneNumberController.text,
              job: jobController.text,
              code: codeController.text,
              address: addressController.text,
              cardType: cardTypeController.text,
          ),
          productId: selectedProduct!,
          promoterId:selectedPromoter == null? promoter.first :selectedPromoter!,
          merchantId:selectedMerchant == null? merchants.first:selectedMerchant!,
          saleType: saleType,
          quantity: double.parse(quantityController.text),
          invoiceDate: paymentDate.toIso8601String(),
          firstInstallmentDate: firstPaymentDate.toIso8601String(),
          paymentCount: double.parse(paymentsCountController.text).toInt(),
          firstPayment: double.parse(firstPaymentController.text),
          isUSD: isUSD,
          selectedDocuments: [],
        );

        // insert invoice into database
        int result = await InvoicesOperations().insertInvoice(invoice);
        if (result != -1) {
          // success
          emit(AddInvoiceFormDone());
          showToast(text: 'تمت إضافة الفاتورة بنجاح', color: Colors.green);
        } else {
          // failure
          showToast(text: 'عذرا فشلت العملية يرجى التأكد من صحة البيانات المدخلة', color: Colors.redAccent);
        }
      } catch (e) {
        showToast(text: "عذرا فشلت العملية يرجى التأكد من صحة البيانات المدخلة", color: Colors.redAccent);
      }

    }
  }

  changeCustomer(String name){
    var find = customers.indexWhere((element) => element.name == name.trim());
    if(find != -1){
      selectedCustomer = customers[find];
      phoneNumberController.text = selectedCustomer!.phoneNumber;
      jobController.text = selectedCustomer!.job;
      codeController.text = selectedCustomer!.code;
      addressController.text =selectedCustomer!.address;
      cardTypeController.text =selectedCustomer!.cardType;
      readOnly = true;
      emit(AddInvoiceFormUpdate());
    }else{
      selectedCustomer = null;
    }
  }
  onClearCustomer(){
    phoneNumberController.text ="";
    jobController.text ="";
    codeController.text = "";
    addressController.text ="";
    cardTypeController.text ="";
    selectedCustomer = null;
    readOnly = false;
    emit(AddInvoiceFormUpdate());
  }



  onClearProduct(){
    selectedProduct = null;
    maxQuantity= 0;
    remainingAmountController.text = "0";
    quantityController.text = "";
    sellingPriceController.text = "";
    purchasePriceController.text = "";
    balanceController.text = "";
    firstPaymentController.text ="";
    remainingAmountController.text = "";
    paymentsCountController.text = "";
    installmentValueController.text = "";
    lastInstallmentValueController.text= "";


    emit(AddInvoiceFormUpdate());
  }
  changeProduct(String name){
    var find = products.indexWhere((element) => element.name == name);
    if(find != -1){
      maxQuantity =  products[find].balance;
      selectedProduct = products[find];
      quantityController.text = "1";
      firstPaymentController.text = "0" ;
      paymentsCountController.text = "1";
      sellingPriceController.text = products[find].sellingPrice.toStringAsFixed(3);
      purchasePriceController.text = products[find].purchasePrice.toStringAsFixed(3);
      remainingAmountController.text = products[find].sellingPrice.toStringAsFixed(3);
      balanceController.text = products[find].balance.toString();
      changePaymentsCount();
      emit(AddInvoiceFormUpdate());
    }else{
      selectedProduct = null;
    }
  }
  onChangeQuantity(String value){
    if(selectedProduct != null &&formKey.currentState!.validate() &&double.tryParse(value)!= null ){
      sellingPriceController.text = (selectedProduct!.sellingPrice * double.parse(value)).toString();
      purchasePriceController.text = (selectedProduct!.purchasePrice * double.parse(value)).toString();
      remainingAmountController.text = sellingPriceController.text;
      changePaymentsCount();
    }
    emit(AddInvoiceFormUpdate());
  }



  changeMerchant(Person? person){
    selectedMerchant = person;
    emit(AddInvoiceFormUpdate());
  }
  changePromoter(Person? person){
    selectedPromoter = person;
    emit(AddInvoiceFormUpdate());
  }


  changeSellInDollars(){
    isUSD = !isUSD;
    emit(AddInvoiceFormUpdate());
  }
  changeSaleType(SaleType value){
    saleType = value;
    emit(AddInvoiceFormUpdate());
  }


  changePaymentsCount(){
    try {
      double amount = double.tryParse(remainingAmountController.text)??0;
      int numberOfInstallments = double.parse(paymentsCountController.text).toInt();
      if(numberOfInstallments == 0) numberOfInstallments=1;
      double installment = amount / numberOfInstallments;
      int roundedInstallment = installment.floor();
      double lastInstallment = amount - (roundedInstallment * (numberOfInstallments - 1));
      installmentValueController.text = roundedInstallment.toStringAsFixed(3);
      lastInstallmentValueController.text = lastInstallment.toStringAsFixed(3) ;
    }catch (e) {
      if (kDebugMode) {
        print("error in changePaymentsCount");
      }
      // TODO
    }

    emit(AddInvoiceFormUpdate());
  }
  onChangeFirstPayment(String value){
    double res = 0;
    double firsP = 0;
    formKey.currentState!.validate();
    try {
      res = double.parse(sellingPriceController.text);
      firsP = double.parse(value);
    } catch (e) {
      if (kDebugMode) {
        print("invalid data");
      }
      return;
    }
    res-=firsP;
    remainingAmountController.text = res.toString();
    changePaymentsCount();
    emit(AddInvoiceFormUpdate());
  }
  changePaymentDate(BuildContext context){
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2000),
      maxTime: DateTime(2100),
      onConfirm: (date) {
        paymentDate = date ;
        paymentDateController.text = DateFormat('yyyy-MM-dd').format(paymentDate);
        emit(AddInvoiceFormUpdate());
      },
      currentTime: paymentDate,
      locale: LocaleType.ar,
    );


  }
  changeFirstPaymentDate(BuildContext context){
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2000),
      maxTime: DateTime(2100),
      onConfirm: (date) {
        firstPaymentDate = date ;
        firstPaymentDateController.text = DateFormat('yyyy-MM-dd').format(firstPaymentDate);
        emit(AddInvoiceFormUpdate());
      },
      currentTime: firstPaymentDate,
      locale: LocaleType.ar,
    );


  }

}
