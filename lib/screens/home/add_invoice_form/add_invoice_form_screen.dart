import 'package:flutter/services.dart';
import 'package:furqan_gallery/local/customer_operations.dart';
import 'package:furqan_gallery/model/customer.dart';
import 'package:furqan_gallery/model/invoice.dart';
import 'package:furqan_gallery/model/person.dart';
import 'package:furqan_gallery/model/thousands_formatter.dart';
import 'package:furqan_gallery/screens/add_person/add_person_s.dart';
import 'package:furqan_gallery/screens/home/tabs/Invoices/invoices_cubit.dart';
import 'package:furqan_gallery/widgets/border_with_title.dart';
import 'package:furqan_gallery/widgets/quantity_input_widget.dart';
import 'package:furqan_gallery/widgets/drop_down_field_with_text_field.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';

import 'add_invoice_form_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddInvoiceFormScreen extends StatelessWidget {
  final InvoicesCubit invoicesCubit;
  const AddInvoiceFormScreen({Key? key, required this.invoicesCubit}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: ()async{
        invoicesCubit.init();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            'إضافة فاتورة',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          centerTitle: true,
          shadowColor: Colors.transparent,
        ),
        body: BlocProvider<AddInvoiceFormCubit>(
          create: (context) => AddInvoiceFormCubit()..init(),
          child: BlocConsumer<AddInvoiceFormCubit, AddInvoiceFormState>(
            listener: (context, state) {
              if(state is AddInvoiceFormDone){
                invoicesCubit.init();
                Navigator.pop(context);
              }
              // TODO: implement listener
            },
            builder: (context, state) {
              return Form(
                key: AddInvoiceFormCubit.get(context).formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //معلومات الزبون
                      BorderWithTitle(
                        title: "معلومات الزبون",
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //اسم الزبون
                            DropDownField(
                              required: true,
                              strict: false,
                              requiredTextError: "الرجاء اختيار زبون",
                              controller: AddInvoiceFormCubit.get(context).customerNameController,
                              labelText: 'الاسم',
                              onChanged: (value){
                                AddInvoiceFormCubit.get(context).changeCustomer(value);
                              },
                              onClear: () {
                                AddInvoiceFormCubit.get(context)
                                    .onClearCustomer();
                              },
                              items: AddInvoiceFormCubit.get(context)
                                  .customers
                                  .map((e) => e.name)
                                  .toList(),
                              onValueChanged: (newValue) {
                                AddInvoiceFormCubit.get(context)
                                    .changeCustomer(newValue);
                              },
                            ),
                            //رقم الزبون
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .phoneNumberController,
                              readOnly: AddInvoiceFormCubit.get(context).readOnly,
                              decoration:
                                  const InputDecoration(labelText: 'رقم الهاتف'),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'الرجاء إدخال رقم الهاتف';
                                }
                                return null;
                              },
                            ),
                            //العمل
                            TextFormField(
                              controller:
                                  AddInvoiceFormCubit.get(context).jobController,
                              readOnly: AddInvoiceFormCubit.get(context).readOnly,
                              decoration: const InputDecoration(labelText: 'العمل'),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'الرجاء إدخال العمل';
                                }
                                return null;
                              },
                            ),
                            //الكود
                            TextFormField(
                              controller:
                                  AddInvoiceFormCubit.get(context).codeController,
                              readOnly: AddInvoiceFormCubit.get(context).readOnly,
                              decoration: const InputDecoration(labelText: 'الكود'),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'الرجاء إدخال الكود';
                                }
                                return null;
                              },
                            ),
                            //العنوان
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .addressController,
                              readOnly: AddInvoiceFormCubit.get(context).readOnly,
                              decoration:
                                  const InputDecoration(labelText: 'العنوان'),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'الرجاء إدخال العنوان';
                                }
                                return null;
                              },
                            ),
                            //نوع البطاقة
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .cardTypeController,
                              readOnly: AddInvoiceFormCubit.get(context).readOnly,
                              decoration:
                                  const InputDecoration(labelText: 'نوع البطاقة'),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'الرجاء إدخال نوع البطاقة';
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                      //تفاصيل الفاتورة
                      BorderWithTitle(
                        title: "تفاصيل الفاتورة",
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(width: 8),
                            const Text(
                              'طريقة البيع:',
                              style: TextStyle(fontSize: 16),
                            ),
                            //طريقة البيع
                            Row(
                              children: [
                                Radio<SaleType>(
                                  value: SaleType.installment,
                                  groupValue:
                                      AddInvoiceFormCubit.get(context).saleType,
                                  onChanged: (value) {
                                    AddInvoiceFormCubit.get(context)
                                        .changeSaleType(value!);
                                  },
                                ),
                                const Text('بالتقسيط'),
                                const SizedBox(width: 8),
                                Radio<SaleType>(
                                  value: SaleType.cash,
                                  groupValue:
                                      AddInvoiceFormCubit.get(context).saleType,
                                  onChanged: (value) {
                                    AddInvoiceFormCubit.get(context)
                                        .changeSaleType(value!);
                                  },
                                ),
                                const Text('نقدي'),
                              ],
                            ),
                            //تاريخ الفاتورة
                            TextFormField(
                              decoration: InputDecoration(
                                labelText: 'تاريخ الفاتورة',
                                suffixIcon: IconButton(
                                  icon: const Icon(Icons.calendar_today),
                                  onPressed: () {
                                    AddInvoiceFormCubit.get(context)
                                        .changePaymentDate(context);
                                  },
                                ),
                              ),
                              onTap: () {
                                AddInvoiceFormCubit.get(context)
                                    .changePaymentDate(context);
                              },
                              readOnly: true,
                              controller: AddInvoiceFormCubit.get(context)
                                  .paymentDateController,
                            ),
                            //بيع بالدولار
                            Row(
                              children: [
                                Checkbox(
                                  value: AddInvoiceFormCubit.get(context).isUSD,
                                  onChanged: (value) {
                                    AddInvoiceFormCubit.get(context)
                                        .changeSellInDollars();
                                  },
                                ),
                                const Text('البيع بالدولار'),
                              ],
                            ),
                            //المروج
                            Row(
                              children: [
                                const Text(
                                  'المروج:',
                                  style: TextStyle(fontSize: 16),
                                ),
                                const SizedBox(width: 20),
                                DropdownButton<Person>(
                                  value: AddInvoiceFormCubit.get(context)
                                      .selectedPromoter,
                                  hint: const Text('بلا',style: TextStyle(color: Colors.black26),),
                                  onChanged: (value) {
                                    AddInvoiceFormCubit.get(context)
                                        .changePromoter(value);
                                  },
                                  items: AddInvoiceFormCubit.get(context)
                                      .promoter
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: Text(e.name),
                                          ))
                                      .toList(),
                                ),
                                const SizedBox(width: 20),
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: () {
                                    AddInvoiceFormCubit.get(context)
                                        .selectedPromoter = null;
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context1) => PersonsScreen(personType: PersonType.promoter,addPaymentFormCubit: AddInvoiceFormCubit.get(context))),
                                    );
                                    },
                                ),
                              ],
                            ),
                            const SizedBox(height: 8),
                            //التاجر
                            Row(
                              children: [
                                const Text(
                                  'التاجر:  ',
                                  style: TextStyle(fontSize: 16),
                                ),
                                const SizedBox(width: 20),
                                DropdownButton<Person>(
                                  value: AddInvoiceFormCubit.get(context)
                                      .selectedMerchant,
                                  hint: const Text('بلا',style: TextStyle(color: Colors.black26),),
                                  onChanged: (value) {
                                    AddInvoiceFormCubit.get(context)
                                        .changeMerchant(value);
                                  },
                                  items: AddInvoiceFormCubit.get(context)
                                      .merchants
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: Text(e.name),
                                          ))
                                      .toList(),
                                ),
                                const SizedBox(width: 20),
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: () {
                                    AddInvoiceFormCubit.get(context)
                                        .selectedMerchant = null;
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context1) =>  PersonsScreen(personType: PersonType.merchant,addPaymentFormCubit: AddInvoiceFormCubit.get(context))),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //المادة
                      BorderWithTitle(
                        title: "تفاصيل المادة",
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //اسم المادة
                            DropDownField(
                              required: true,
                              controller: AddInvoiceFormCubit.get(context)
                                  .productNameController,
                              strict: true,
                              labelText: 'المادة',
                              requiredTextError: "الرجاء اختيار مادة",
                              onValueChanged: (newValue) {
                                AddInvoiceFormCubit.get(context)
                                    .changeProduct(newValue);
                              },

                              onClear: () {
                                AddInvoiceFormCubit.get(context).onClearProduct();
                              },
                              items: AddInvoiceFormCubit.get(context)
                                  .products
                                  .map((e) => e.name)
                                  .toList(),
                            ),
                            //الكمية
                            QuantityInputWidget(
                              maxQuantity:
                                  AddInvoiceFormCubit.get(context).maxQuantity,
                              title: "الكمية",
                              error: AddInvoiceFormCubit.get(context).errorTextQuantity,

                              validator: (value) {
                                double? newQuantity = double.tryParse(value);
                                if(newQuantity == null || newQuantity <= 0){
                                  if(value.isEmpty){
                                    return "هذه الخانة إلزامية";
                                  }else{
                                    return "يجب تحديد كمية اكبر تماماً من 0";
                                  }
                                }
                                if(newQuantity > AddInvoiceFormCubit.get(context).maxQuantity){
                                  return "لا يتوفر رصيد كافي من المادة (${AddInvoiceFormCubit.get(context).maxQuantity})";
                                }else if(newQuantity < 0){
                                  return "قيمة عير صالحة";
                                }
                                return null;
                              },
                              onChange: (value) {
                                AddInvoiceFormCubit.get(context)
                                    .onChangeQuantity(value);
                              },
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .quantityController,
                            ),
                            //سعر المبيع
                            const Text(
                              'سعر المبيع:',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .sellingPriceController,
                              readOnly: true,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly,
                                ThousandsFormatter(),
                              ],
                              decoration: const InputDecoration(
                                hintText: '0',
                              ),
                              keyboardType: TextInputType.number,
                            ),
                            const SizedBox(height: 16),
                            //سعر الكلفة
                            const Text(
                              'سعر الكلفة:',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .purchasePriceController,
                              readOnly: true,
                              decoration: const InputDecoration(
                                hintText: '0',
                              ),
                              keyboardType: TextInputType.number,
                            ),
                            const SizedBox(height: 16),
                            //الرصيد
                            const Text(
                              'الرصيد:',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            TextFormField(
                              controller: AddInvoiceFormCubit.get(context)
                                  .balanceController,
                              readOnly: true,
                              decoration: const InputDecoration(
                                hintText: '0',
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ],
                        ),
                      ),
                      //الدفعات
                      BorderWithTitle(
                        title: "الدفعات",
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //الدفعة الاولى
                            QuantityInputWidget(
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .firstPaymentController,
                              maxQuantity: double.tryParse(
                                      AddInvoiceFormCubit.get(context)
                                          .sellingPriceController
                                          .text) ??
                                  0,
                              title: "الدفعة الاولى",
                              validator: (value) {
                                double? newQuantity = double.tryParse(value);
                                double mx = double.tryParse(
                                    AddInvoiceFormCubit.get(context).sellingPriceController.text) ?? 1;

                                if(newQuantity == null){
                                  if(value.isEmpty){
                                    return null;
                                  }else{
                                    return "قيمة عير صالحة";
                                  }
                                }
                                if(newQuantity > mx){
                                  return "يجب ان لا تتجاوز الدفعة الاولى قيمة السعر ($mx)";
                                }else if(newQuantity < 0){
                                  return "قيمة عير صالحة";
                                }
                                return null;
                              },
                              onChange: (value) {
                                AddInvoiceFormCubit.get(context)
                                    .onChangeFirstPayment(value);
                              },
                            ),
                            //المبلغ المتبقي
                            QuantityInputWidget(
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .remainingAmountController,
                              maxQuantity: double.tryParse(
                                      AddInvoiceFormCubit.get(context)
                                          .sellingPriceController
                                          .text) ??
                                  0,
                              validator: (value) {
                                return null;
                              },
                              readOnly: true,
                              title: "المبلغ المتبقي",
                            ),
                            //عدد الدفات
                            QuantityInputWidget(
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .paymentsCountController,
                              title: "عدد الدفعات",
                              isIntValue: true,
                              validator: (value) {
                                double? newQuantity = double.tryParse(value);
                                if(newQuantity == null || newQuantity < 1){
                                  if(value.isEmpty){
                                    return null;
                                  }else{
                                    return "قيمة غير صالحة يجب ان لا تقل عن دفعة واحدة";
                                  }
                                }
                                return null;
                              },
                              onChange: (value) {
                                AddInvoiceFormCubit.get(context).changePaymentsCount();
                                AddInvoiceFormCubit.get(context).formKey.currentState!.validate();
                              },
                            ),
                            //مبلغ القسط
                            QuantityInputWidget(
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .installmentValueController,
                              readOnly: true,
                              validator: (value) {
                                return null;
                              },
                              title: "مبلغ القسط",
                            ),
                            //مبلغ القسط الاخير
                            QuantityInputWidget(
                              textEditingController:
                                  AddInvoiceFormCubit.get(context)
                                      .lastInstallmentValueController,
                              validator: (value) {
                                return null;
                              },
                              readOnly: true,
                              title: "مبلغ القسط الاخير",
                            ),

                            //تاريخ اول دفعة
                            TextFormField(
                              decoration: InputDecoration(
                                labelText: 'تاريخ اول قسط',
                                suffixIcon: IconButton(
                                  icon: const Icon(Icons.calendar_today),
                                  onPressed: () {
                                    AddInvoiceFormCubit.get(context)
                                        .changeFirstPaymentDate(context);
                                  },
                                ),
                              ),
                              onTap: () {
                                AddInvoiceFormCubit.get(context)
                                    .changeFirstPaymentDate(context);
                              },
                              readOnly: true,
                              controller: AddInvoiceFormCubit.get(context)
                                  .firstPaymentDateController,
                            ),
                          ],
                        ),
                      ),

                      Padding(
                        padding:
                            const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        child: SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color?>(
                                (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Colors.purple.withOpacity(0.5);
                                  } else {
                                    return Colors.deepPurple;
                                  }
                                },
                              ),
                            ),
                            onPressed: () {
                              if (AddInvoiceFormCubit.get(context)
                                  .formKey
                                  .currentState!
                                  .validate()) {
                                AddInvoiceFormCubit.get(context).addInvoice();
                              }
                            },
                            child: const Padding(
                              padding: EdgeInsets.symmetric(vertical: 12.0),
                              child: Text(
                                'حفظ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
