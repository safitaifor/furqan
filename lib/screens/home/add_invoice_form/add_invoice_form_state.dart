part of 'add_invoice_form_cubit.dart';

abstract class AddInvoiceFormState {}

class AddInvoiceFormInitial extends AddInvoiceFormState {}

class AddInvoiceFormLoading extends AddInvoiceFormState {}

class AddInvoiceFormError extends AddInvoiceFormState {}

class AddInvoiceFormDone extends AddInvoiceFormState {}

class AddInvoiceFormUpdate extends AddInvoiceFormState {}
