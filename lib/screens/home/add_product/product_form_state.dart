part of 'product_form_cubit.dart';

abstract class ProductFormState {}

class ProductFormInitial extends ProductFormState {}

class ProductFormLoading extends ProductFormState {}

class ProductFormError extends ProductFormState {}

class ProductFormDone extends ProductFormState {}

class ProductFormDeleted extends ProductFormState {}
class ProductFormUpdated extends ProductFormState {}

class ProductFormUpdate extends ProductFormState {}
