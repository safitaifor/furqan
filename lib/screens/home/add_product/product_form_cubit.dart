import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:furqan_gallery/local/product_operations.dart';
import 'package:furqan_gallery/model/product.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';

part 'product_form_state.dart';

class ProductFormCubit extends Cubit<ProductFormState> {
  ProductFormCubit() : super(ProductFormInitial());

  static ProductFormCubit get(context) => BlocProvider.of(context);
  late TextEditingController nameController;
  late TextEditingController purchasePriceController;
  late TextEditingController sellingPriceController;
  late TextEditingController balanceController;
  late TextEditingController barcodeController;
  late TextEditingController quantityInController;
  late TextEditingController quantityOutController;
  late TextEditingController initialBalanceController ;
  late Product product;
  late GlobalKey<FormState> formKey;
  bool readOnly = false;



  void init(Product? product){
    formKey = GlobalKey<FormState>();
    nameController = TextEditingController();
    purchasePriceController = TextEditingController();
    sellingPriceController = TextEditingController();
    balanceController = TextEditingController();
    barcodeController = TextEditingController();
    quantityInController = TextEditingController();
    quantityOutController = TextEditingController();
    initialBalanceController = TextEditingController();
    if(product != null){
      readOnly = true;
      this.product = product;
      nameController.text = product.name;
      purchasePriceController.text = product.purchasePrice.toString();
      sellingPriceController.text = product.sellingPrice.toString();
      balanceController.text = product.balance.toString();
      barcodeController.text = product.barcode ;
      quantityInController.text = product.quantityIn.toString();
      quantityOutController.text= product.quantityOut.toString();
      initialBalanceController.text = product.initialBalance.toString();
    }

  }

  addProduct()async {
    emit(ProductFormLoading());

    try {
      final name = nameController.text;
      final purchasePrice = double.parse(purchasePriceController.text);
      final sellingPrice = double.parse(sellingPriceController.text);
      final barcode = barcodeController.text;
      final quantityOut = int.tryParse(quantityOutController.text)??0;
      final initialBalance = double.parse(initialBalanceController.text);
      final quantityIn = int.tryParse(quantityInController.text)??initialBalance.toInt();
      final balance = double.tryParse(balanceController.text)??initialBalance;



    product = Product(
      name: name,
      purchasePrice: purchasePrice,
      sellingPrice: sellingPrice,
      balance: balance,
      barcode: barcode,
      quantityIn: quantityIn,
      quantityOut: quantityOut,
      initialBalance: initialBalance,
    );


    } on Exception catch (e) {
      emit(ProductFormError());
    }
    ProductOperations productOperations = ProductOperations();
     await productOperations.insertProduct(product);
   // showErrorToast(error: "error");
    emit(ProductFormDone());

  }

  changeStateEditing(Product? product){
    readOnly = !readOnly;
    if(product != null){
      this.product = product;
      nameController.text = product.name;
      purchasePriceController.text = product.purchasePrice.toString();
      sellingPriceController.text = product.sellingPrice.toString();
      balanceController.text = product.balance.toString();
      barcodeController.text = product.barcode ;
      quantityInController.text = product.quantityIn.toString();
      quantityOutController.text= product.quantityOut.toString();
      initialBalanceController.text = product.initialBalance.toString();
    }
    emit(ProductFormUpdate());

  }

  deleteProduct()async{
    emit(ProductFormLoading());
    ProductOperations productOperations = ProductOperations();
    await productOperations.deleteProduct(product.id!).catchError((error){
      emit(ProductFormError());
    });
    emit(ProductFormDeleted());

  }
  updateProduct()async {
    emit(ProductFormLoading());

    try {
      final name = nameController.text;
      final purchasePrice = double.parse(purchasePriceController.text);
      final sellingPrice = double.parse(sellingPriceController.text);
      final barcode = barcodeController.text;
      final quantityOut = int.tryParse(quantityOutController.text)??0;
      final initialBalance = double.parse(initialBalanceController.text);
      final quantityIn = int.tryParse(quantityInController.text)??initialBalance.toInt();
      final balance = double.tryParse(balanceController.text)??initialBalance;

      int? id = product.id;

      product = Product(
        id: id,
        name: name,
        purchasePrice: purchasePrice,
        sellingPrice: sellingPrice,
        balance: balance,
        barcode: barcode,
        quantityIn: quantityIn,
        quantityOut: quantityOut,
        initialBalance: initialBalance,
      );


    } on Exception catch (e) {
      emit(ProductFormError());
    }
    ProductOperations productOperations = ProductOperations();
    await productOperations.updateProduct(product);
    emit(ProductFormUpdated());

  }
}
