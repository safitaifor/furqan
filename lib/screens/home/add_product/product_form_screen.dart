import 'package:furqan_gallery/model/product.dart';
import 'package:furqan_gallery/screens/home/tabs/store/store_cubit.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';

import 'product_form_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductFormScreen extends StatelessWidget {
  final Product? product;
  final StoreCubit storeCubit;
  const ProductFormScreen({Key? key, this.product, required this.storeCubit, }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery
        .of(context)
        .size
        .height;
    double width = MediaQuery
        .of(context)
        .size
        .width;
    ProductFormCubit cubit = ProductFormCubit();


    return WillPopScope(
      onWillPop: ()async{
        storeCubit.init();
        return true;
      },

      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            'إضافة مادة',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          actions: [
            if(product != null)
            BlocProvider<ProductFormCubit>(
              create: (context) => cubit,
              child: BlocConsumer<ProductFormCubit, ProductFormState>(
                listener: (context, state) {

                  // TODO: implement listener
                },
                builder: (context, state) {
                  return IconButton(
                    icon:ProductFormCubit.get(context).readOnly?const Icon(Icons.edit):const Icon(Icons.cancel_outlined),
                    onPressed: (){

                      ProductFormCubit.get(context).changeStateEditing(product);
                    },
                  );
                },
              ),
            )
          ],
          centerTitle: true,
          shadowColor: Colors.transparent,
        ),
        body: BlocProvider<ProductFormCubit>(
          create: (context) => cubit..init(product),
          child: BlocConsumer<ProductFormCubit, ProductFormState>(
            listener: (context, state) {
              if(state is ProductFormDone) {
                showToast(text: "تمت إضافة المنتج بنجاح");
                storeCubit.init();
                Navigator.of(context).pop();
              }
              if(state is ProductFormDeleted) {
                showToast(text: "تم حذف المنتج المنتج بنجاح");
                storeCubit.init();
                Navigator.of(context).pop();
              }

              if(state is ProductFormUpdated) {
                showToast(text: "تم تحديث المنتج المنتج بنجاح");
                storeCubit.init();
                Navigator.of(context).pop();
              }

              if(state is ProductFormError) {
                showToast(text: "حدث خطأ الرجاء التأكد من صحة البياتات",color: Colors.redAccent);
              }
              // TODO: implement listener
            },
            builder: (context, state) {
              return Form(
                key: ProductFormCubit.get(context).formKey,
                child: Container(
                  padding:const EdgeInsets.all(6),
                  height: height,
                  width: width,
                  child: ListView(
                    children: [
                      const Text(
                        'اسم المنتج:',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        controller: ProductFormCubit.get(context).nameController,
                        readOnly: ProductFormCubit.get(context).readOnly,
                        decoration: const InputDecoration(
                          hintText: 'أدخل اسم المنتج',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'يرجى إدخال اسم المنتج';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16),
                      const Text(
                        'سعر الشراء:',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        controller: ProductFormCubit.get(context).purchasePriceController,
                        readOnly: ProductFormCubit.get(context).readOnly,
                        decoration: const InputDecoration(
                          hintText: 'أدخل سعر الشراء',
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'يرجى إدخال سعر الشراء';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16),
                      const Text(
                        'سعر البيع:',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        controller: ProductFormCubit.get(context).sellingPriceController,
                        readOnly: ProductFormCubit.get(context).readOnly,
                        decoration: const InputDecoration(
                          hintText: 'أدخل سعر البيع',
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'يرجى إدخال سعر البيع';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16),
                      const Text(
                        'الرصيد الأول:',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        controller: ProductFormCubit.get(context).initialBalanceController,
                        readOnly: ProductFormCubit.get(context).readOnly,
                        decoration: const InputDecoration(
                          hintText: 'أدخل الرصيد الأول',
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'يرجى إدخال الرصيد الأول';
                          }
                          return null;
                        },
                      ),
                      if(product!= null)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 16),
                          const Text(
                            'الرصيد:',
                            style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            controller: ProductFormCubit.get(context).balanceController,
                            readOnly: true,
                            decoration: const InputDecoration(
                              hintText: 'أدخل الرصيد',
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      const Text(
                        'الباركود:',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        controller: ProductFormCubit.get(context).barcodeController,
                        readOnly: ProductFormCubit.get(context).readOnly,
                        decoration: const InputDecoration(
                          hintText: 'أدخل الباركود',
                        ),
                      ),
                      if(product!= null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 16),
                          const Text(
                            'كمية الوارد:',
                            style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            controller: ProductFormCubit.get(context).quantityInController,
                            readOnly: true,
                            decoration: const InputDecoration(
                              hintText: 'أدخل كمية الوارد',
                            ),
                            keyboardType: TextInputType.number,

                          ),
                        ],
                      ),
                      if(product!=null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 16),
                          const Text(
                            'كمية الخارج:',
                            style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            controller: ProductFormCubit.get(context).quantityOutController,
                            readOnly: true,
                            decoration: const InputDecoration(
                              hintText: 'أدخل كمية الخارج',
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ],
                      ),
                      const SizedBox(height: 32),
                      if(!ProductFormCubit.get(context).readOnly)
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
                                (Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return  Colors.purple.withOpacity(0.5);
                              } else {
                                return state is! ProductFormLoading? Colors.deepPurple: Colors.grey.withOpacity(0.5);
                              }
                            },
                          ),
                        ),
                        onPressed: ()async {
                          if (ProductFormCubit.get(context).formKey.currentState!.validate() && state is! ProductFormLoading) {
                            if(product == null) {
                              await ProductFormCubit.get(context).addProduct();
                            }else{
                              await ProductFormCubit.get(context).updateProduct();
                            }
                          }
                        },
                        child:const Padding(
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          child: Text('حفظ',style: TextStyle(fontWeight:FontWeight.bold, fontSize: 16),),
                        ),
                      ),
                      if(product != null)
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
                                (Set<MaterialState> states) {
                              if (states.contains(MaterialState.pressed)) {
                                return  Colors.red.withOpacity(0.5);
                              } else {
                                return state is! ProductFormLoading? Colors.red: Colors.grey.withOpacity(0.5);
                              }
                            },
                          ),
                        ),
                        onPressed: ()async{
                          if (state is! ProductFormLoading &&  ProductFormCubit.get(context).formKey.currentState!.validate()) {
                           await ProductFormCubit.get(context).deleteProduct();
                          }
                        },
                        child:const Padding(
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          child: Text('حذف',style: TextStyle(fontWeight:FontWeight.bold, fontSize: 16),),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}


