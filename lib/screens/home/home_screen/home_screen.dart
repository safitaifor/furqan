import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/screens/home/add_invoice_form/add_invoice_form_screen.dart';
import 'package:furqan_gallery/screens/home/add_product/product_form_screen.dart';
import 'package:furqan_gallery/screens/home/tabs/Invoices/invoices_cubit.dart';
import 'package:furqan_gallery/screens/home/tabs/Invoices/invoices_screen.dart';
import 'package:furqan_gallery/screens/home/tabs/store/store_cubit.dart';
import 'package:furqan_gallery/screens/home/tabs/store/store_screen.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{

  late TabController tabController;
  int currentIndex = 0;
  late StoreCubit storeCubit;
  late InvoicesCubit invoicesCubit;

  List <String> titles=[
    'المخزن',
    'الدفعات',
  ];


  @override
  void initState() {
    storeCubit = StoreCubit();
    invoicesCubit = InvoicesCubit();
    tabController = TabController(
      length: 2,
      vsync: this,
      initialIndex: 0,
      animationDuration: const Duration(milliseconds: 500),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider<StoreCubit>(
          create: (context) => storeCubit..init(),
        ),
        BlocProvider<InvoicesCubit>(
          create: (context) => invoicesCubit..init(),
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            titles[currentIndex],
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          centerTitle: true,
          shadowColor: Colors.transparent,
        ),
        drawer: const Drawer(
          backgroundColor: Colors.deepPurple,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if(currentIndex == 0){
             Navigator.push(context, MaterialPageRoute(builder: (context) =>  ProductFormScreen(storeCubit:storeCubit,)));
            }else{
             Navigator.push(context, MaterialPageRoute(builder: (context) =>  AddInvoiceFormScreen(invoicesCubit: invoicesCubit,)));
            }
          },
          child: Icon(currentIndex==1? Icons.add_card_rounded :Icons.add),
        ),
        body: TabBarView(
          physics:const NeverScrollableScrollPhysics(),
          controller: tabController,
          children: const [
            StoreScreen(),
            InvoicesScreen(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentIndex,
          backgroundColor: Colors.deepPurple,
          onTap: (value){
            setState((){
              currentIndex=value;
              tabController.animateTo(value);
            });
          },
          selectedItemColor: Colors.grey,
          unselectedItemColor: Colors.white,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.store,),
              label: 'المخزن'
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.payments_outlined,),
                label: 'الدفعات'
            ),
          ],
        ),
      ),
    );
  }
}
