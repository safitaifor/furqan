import 'package:flutter/material.dart';
import 'package:furqan_gallery/local/person_operations.dart';
import 'package:furqan_gallery/model/person.dart';
import 'package:furqan_gallery/screens/home/add_invoice_form/add_invoice_form_cubit.dart';


class PersonsScreen extends StatefulWidget {
  final PersonType personType ;
  final AddInvoiceFormCubit addPaymentFormCubit;
  const PersonsScreen({Key? key, required this.personType, required this.addPaymentFormCubit}) : super(key: key);

  @override
  State<PersonsScreen> createState() => _PersonsScreenState();
}

class _PersonsScreenState extends State<PersonsScreen> {
  late PersonType _personType;
  List<Person> _persons =[];
  final _personNameController = TextEditingController();
  Person? _selectedPerson;
  late GlobalKey<FormState> formKey;
  late PersonOperations personOperations ;

  @override
  void initState() {
    _personType = widget.personType;
    personOperations = PersonOperations();
    formKey = GlobalKey<FormState>();
    getPersons();

    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    _personNameController.dispose();
    super.dispose();
  }

  void getPersons()async{
    _persons =
        await personOperations.getAllPersons(_personType).then((value) {
      setState(() {});
      return value;
    });
  }

  void _showEditPersonDialog(Person merchant) {
    _personNameController.text = merchant.name;
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('تعديل ${_personType == PersonType.promoter?"المروج":"التاجر"}'),
          content: TextField(
            controller: _personNameController,
            decoration: const InputDecoration(
              labelText: 'اسم التاجر',
            ),
          ),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('إلغاء'),
            ),
            FlatButton(
              onPressed: () {
                final newMerchantName = _personNameController.text;
                setState(() {
                  merchant.name = newMerchantName;
                });
                Navigator.pop(context);
              },
              child: const Text('حفظ'),
            ),
          ],
        );
      },
    );
  }

  void _showDeletePersonsDialog(Person merchant) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('حذف ${_personType == PersonType.promoter?"المروج":"التاجر"}'),
          content: Text('هل أنت متأكد من رغبتك في حذف ${merchant.name}?'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('إلغاء'),
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  personOperations.deletePerson(merchant.id!, _personType);
                  if(merchant.id!=1){
                    _persons.remove(merchant);
                  }
                });
                Navigator.pop(context);
              },
              child: const Text('حذف'),
            ),
          ],
        );
      },
    );
  }

  void _deleteRenewedPersons() {

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('حذف ${_personType == PersonType.promoter?"المروجين":"التجار"}'),
          content: const Text('هل أنت متأكد سيتم حذف جميع العناصر امحددة؟'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('إلغاء'),
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  _persons.removeWhere((person) {
                    if(person.selected) {
                      personOperations.deletePerson(person.id!, _personType);
                    }
                    return person.selected && person.id != 1;
                  });
                });
                Navigator.pop(context);
              },
              child: const Text('حذف'),
            ),
          ],
        );
      },
    );



  }

  void _addNewPerson() {
    if (formKey.currentState!.validate()) {
      final newMerchantName = _personNameController.text;
      setState(()async {
        await personOperations
            .insertPerson(Person(name: newMerchantName, type: _personType))
            .then((value) => getPersons());
        _personNameController.clear();
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        if(_personType == PersonType.promoter) {
          widget.addPaymentFormCubit.updatePromotersOnly();
        }else{
          widget.addPaymentFormCubit.updateMerchantsOnly();
        }
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(_personType == PersonType.promoter?'المروجين' :'التجار'),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: _deleteRenewedPersons,
            ),
          ],
        ),
        body: Column(
          children: [
            Form(
              key:formKey ,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextFormField(
                  controller: _personNameController,
                  autofocus: true,
                  validator: (value){
                    if(value == null || value.isEmpty){
                      return "ادخل اسم التاجر اولا";
                    }else if(value.length<3){
                      return "الاسم قصير جدا";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'اسم ${_personType == PersonType.promoter?"المروج":"التاجر"} الجديد',
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.add),
                      onPressed: _addNewPerson,
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _persons.length,
                itemBuilder: (context, index) {
                  final merchant = _persons[index];
                  return ListTile(
                    title: Text(merchant.name),
                    leading: Checkbox(
                      value: merchant.selected,
                      onChanged: (value) {
                        setState(() {
                          merchant.selected = value??false;
                        });
                      },
                    ),
                    trailing: IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        _showEditPersonDialog(merchant);
                      },
                    ),
                    onLongPress: () {
                      _showDeletePersonsDialog(merchant);
                    },
                    onTap: () {
                      setState(() {
                        _selectedPerson = merchant;
                      });
                    },
                    selected: _selectedPerson == merchant,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}