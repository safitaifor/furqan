
import 'package:flutter/material.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';


class PdfViewScreen extends StatefulWidget {
  final String pdfPath;
  const PdfViewScreen({Key? key, required this.pdfPath}) : super(key: key);

  @override
  State<PdfViewScreen> createState() => _PdfViewScreenState();
}

class _PdfViewScreenState extends State<PdfViewScreen> {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(builder: (context) {
        return Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: const Text('الفاتورة'),
            centerTitle: true,
            actions: [
              IconButton(onPressed: (){

              }, icon:const Icon(Icons.share))
            ],
          ),
          body: PdfView(path: widget.pdfPath),
        );
      }),
    );
  }
}