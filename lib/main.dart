import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:furqan_gallery/model/bloc_observer.dart';
import 'package:furqan_gallery/remote/dio_helper.dart';

import 'screens/splash/splash_screen.dart';

void main() async{
  BlocOverrides.runZoned(() {
    // Use cubits...
  }, blocObserver: MyBlocObserver());
  DioHelper.init();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp(),);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            fontFamily: 'Tajawal',
            primarySwatch: Colors.deepPurple
        ),
        builder: (context, child) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child:child!,
          );
        },
        home:const SplashScreen(),

      ),
    );
  }
}
