
enum PersonType {promoter , merchant}

class Person {
  String name;
  int? id;
  PersonType type;
  late final String personTypeText;
  bool selected;
  bool renewed;
  Person({required this.name, required this.type, this.id ,this.selected = false ,this.renewed = false}){
    if(type == PersonType.promoter){
      personTypeText = "مروج";
    }else{
      personTypeText = "تاجر";
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
    };
  }

  static Person fromMap(Map<String, dynamic> map ,PersonType type) {
    return Person(
      type: type,
      name: map['name'],
      id: map['_id'],
    );
  }
}