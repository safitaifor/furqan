
import 'package:furqan_gallery/local/customer_operations.dart';
import 'package:furqan_gallery/local/invoice_document_operations.dart';
import 'package:furqan_gallery/local/person_operations.dart';
import 'package:furqan_gallery/local/product_operations.dart';
import 'package:furqan_gallery/model/customer.dart';
import 'package:furqan_gallery/model/document.dart';
import 'package:furqan_gallery/model/person.dart';
import 'package:furqan_gallery/model/product.dart';

enum SaleType {
  cash,
  installment,
  other,
}

extension SaleTypeExtension on SaleType {
  String get nameAr {
    switch (this) {
      case SaleType.cash:
        return 'كاش';
      case SaleType.installment:
        return 'تقسيط';
      case SaleType.other:
        return 'أخرى';
      default:
        return '';
    }
  }
  bool get value {
    switch (this) {
      case SaleType.cash:
        return false;
      case SaleType.installment:
        return true;
      default:
        return false;
    }
  }

  int get intValue {
    switch (this) {
      case SaleType.cash:
        return 0;
      case SaleType.installment:
        return 1;
      default:
        return 0;
    }
  }

}


class Invoice {
  int? id;
  Customer customerId;
  List<Document> selectedDocuments;
  Product productId;
  Person? promoterId;
  Person? merchantId;
  int paymentCount;
  double firstPayment;
  String invoiceDate;
  String firstInstallmentDate;
  SaleType saleType;
  double quantity;
  bool isUSD;
  late double installmentValue;
  late double lastInstallmentValue;

  Invoice({
    this.id,
    required this.quantity,
    required this.saleType,
    required this.customerId,
    required this.selectedDocuments,
    required this.productId,
    this.promoterId,
    this.merchantId,
    required this.paymentCount,
    required this.firstPayment,
    required this.invoiceDate,
    required this.firstInstallmentDate,
    required this.isUSD,
  }){
    double amount = (productId.sellingPrice * quantity )- firstPayment;
    int numberOfInstallments = paymentCount==0?1:paymentCount;
    if(numberOfInstallments == 0) numberOfInstallments=1;
    double installment = amount / numberOfInstallments;
    int roundedInstallment = installment.floor();
    double lastInstallment = amount - (roundedInstallment * (numberOfInstallments - 1));
    installmentValue = roundedInstallment.toDouble();
    lastInstallmentValue = lastInstallment;
  }

  // تحويل الفاتورة إلى Map
  Map<String, dynamic> toMap() {
    return {
      'customer_id': customerId.id,
      'document_id': -1,//selectedDocuments,
      'product_id': productId.id,
      'promoter_id': promoterId!.id,
      'merchant_id': merchantId!.id,
      'payment_count': paymentCount,
      'first_payment': firstPayment,
      'invoice_date': invoiceDate,
      'first_installment_date': firstInstallmentDate,
      'is_cash': saleType.intValue,
      'is_usd': isUSD ? 1 : 0,
      'quantity':quantity,
    };
  }

  // تحويل Map إلى فاتورة
  static Future<Invoice> fromMap(Map<String, dynamic> map)async{
    PersonOperations personOperations = PersonOperations();
    CustomerOperations customerOperations = CustomerOperations();
    ProductOperations  productOperations = ProductOperations();
    InvoiceDocumentOperations invoiceDocumentOperations = InvoiceDocumentOperations();

    return Invoice(
      id: map['_id'],
      customerId:await customerOperations.getCustomerById(map['customer_id'])??Customer(id:-1,name: "محذوف", phoneNumber: "محذوف", job: "محذوف", code: "محذوف", address: "محذوف", cardType: "محذوف"),
      selectedDocuments:await invoiceDocumentOperations.getInvoiceDocumentsByInvoiceId(map['_id']),
      productId:await productOperations.getProductById(map['product_id'])??Product(id:-1,name:  "محذوف", purchasePrice:  0, sellingPrice:  0, balance:  0, barcode:  "محذوف", quantityIn:  0, quantityOut:  0, initialBalance:  0),
      promoterId:await personOperations.getPersonById(map['promoter_id'], PersonType.promoter),
      merchantId:await personOperations.getPersonById(map['merchant_id'], PersonType.merchant),
      paymentCount: map['payment_count'],
      firstPayment: map['first_payment'],
      invoiceDate: map['invoice_date'],
      firstInstallmentDate: map['first_installment_date'],
      saleType: map['is_cash'] == 1?SaleType.installment:SaleType.cash,
      isUSD: map['is_usd'] == 1,
      quantity: map["quantity"],
    );
  }
}