
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class ThousandsFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    // يتم تحويل القيمة إلى نص
    String value = newValue.text;

    // يتم إزالة أي فواصل الآلاف الحالية
    value = value.replaceAll(',', '');

    // يتم إضافة فواصل الآلاف الجديدة
    if (value.isNotEmpty) {
      final number = int.parse(value);
      value = NumberFormat("#,###").format(number);
    }

    // يتم إعادة تنسيق القيمة النصية الجديدة إلى TextEditingValue وإعادتها
    return TextEditingValue(
      text: value,
      selection: TextSelection.collapsed(offset: value.length),
    );
  }
}