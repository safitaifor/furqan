class InvoiceDocument {
  int? id;
  final int invoiceId;
  final int documentId;
  final String agreement;

  InvoiceDocument({
    required this.invoiceId,
    required this.documentId,
    this.id,
    required this.agreement,

  });

  Map<String, dynamic> toMap() {
    return {
      'invoice_id': invoiceId,
      'document_id': documentId,
      'agreement': agreement,
    };
  }

  factory InvoiceDocument.fromMap(Map<String, dynamic> map) {
    return InvoiceDocument(
      invoiceId: map['invoice_id'],
      documentId: map['document_id'],
      id: map['_id'],
      agreement: map['agreement'],
    );
  }
}
