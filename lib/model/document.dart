

class Document {
  final int? id;
  final String name;

  Document({
    this.id,
    required this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
    };
  }

  factory Document.fromMap(Map<String, dynamic> map) {
    return Document(
      id: map['_id'],
      name: map['name'],
    );
  }
}