
class Product {
  String name;
  double purchasePrice;
  double sellingPrice;
  double balance;
  String barcode;
  int quantityIn;
  int quantityOut;
  double initialBalance;
  int? id;

  Product({
    required this.name,
    required this.purchasePrice,
    required this.sellingPrice,
    required this.balance,
    required this.barcode,
    required this.quantityIn,
    required this.quantityOut,
    required this.initialBalance,
    this.id,
  });

  factory Product.fromMap(Map<String, dynamic> json) {
    return Product(
      name: json['name'],
      purchasePrice: json['purchase_price'],
      sellingPrice: json['selling_price'],
      balance: json['initial_balance'] +json['quantity_in'] -  json['quantity_out'],
      barcode: json['barcode'],
      quantityIn: json['quantity_in'],
      quantityOut: json['quantity_out'],
      initialBalance: json['initial_balance'],
      id: json["_id"],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'purchase_price': purchasePrice,
      'selling_price': sellingPrice,
      'barcode': barcode,
      'quantity_in': quantityIn,
      'quantity_out': quantityOut,
      'initial_balance': initialBalance,
    };
  }
}