
class Customer {
  String name;
  String phoneNumber;
  String job;
  String code;
  String address;
  String cardType;
  int? id;
  Customer({
    required this.name,
    required this.phoneNumber,
    required this.job,
    required this.code,
    required this.address,
    required this.cardType,
    this.id,
  });

  // تحويل كائن الزبون إلى Map
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'phone_number': phoneNumber,
      'job': job,
      'code': code,
      'address': address,
      'card_type': cardType,
    };
  }

  // تحويل Map إلى كائن الزبون
  static Customer fromMap(Map<String, dynamic> map) {
    return Customer(
      name: map['name'],
      phoneNumber: map['phone_number'],
      job: map['job'],
      code: map['code'],
      address: map['address'],
      cardType: map['card_type'],
      id: map["_id"],
    );
  }
}