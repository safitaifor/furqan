import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/model/customer.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:sqflite/sqflite.dart';

class CustomerOperations {
  var database = DatabaseHelper.instance.database;

  // insert a customer into the table
  Future<int> insertCustomer(Customer customer) async {
    try {
      Database db = await database;
      return await db.insert(DatabaseHelper.tableCustomers, customer.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        // يوجد قيمة مكررة في الحقل الذي يحتوي على الفهرس الفريد
        if (kDebugMode) {
          showToast(text: 'عذراً يوجد زبون بنفس الاسم', color: Colors.redAccent);
        }
      } else {
        // خطأ آخر
        if (kDebugMode) {
          print('خطأ: ${e.toString()}');
        }
      }
      return -1;
    }
  }

  // update a customer in the table
  Future<int> updateCustomer(Customer customer) async {
    Database db = await database;
    return await db.update(DatabaseHelper.tableCustomers, customer.toMap(),
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [customer.id!]);
  }

  // delete a customer from the table
  Future<int> deleteCustomer(int id) async {
    Database db = await database;
    return await db.delete(DatabaseHelper.tableCustomers, where: '${DatabaseHelper.columnId} = ?', whereArgs: [id]);
  }

  // get a customer from the table by id
  Future<Customer?> getCustomerById(int id) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableCustomers,
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [id], limit: 1);
    if (maps.isNotEmpty) {
      return Customer.fromMap(maps.first);
    } else {
      return null;
    }
  }

  // get all customers from the table
  Future<List<Customer>> getAllCustomers() async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableCustomers);
    return List.generate(maps.length, (i) {
      return Customer.fromMap(maps[i]);
    });
  }
}