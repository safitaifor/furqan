
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static const _databaseName = "furqan_database.db";
  static const _databaseVersion = 1;

  // تعريف اسماء الجداول
  static const productsTable = 'products_table';
  static const tableCustomers = 'customers_table';
  static const tablePromoter = 'promoter_table';
  static const tableMerchant = 'merchant_table';
  static const tableDocuments = 'documents_table';
  static const tableInvoices = 'invoices';
  static const tableInvoiceDocument = 'invoice_document_table';

  // تعريف متغيرات المادة
  static const columnId = '_id';
  static const columnName = 'name';
  static const columnPurchasePrice = 'purchase_price';
  static const columnSellingPrice = 'selling_price';
  static const columnBarcode = 'barcode';
  static const columnQuantityIn = 'quantity_in';
  static const columnQuantityOut = 'quantity_out';
  static const columnInitialBalance = 'initial_balance';

  // تعريف متغيرات الزبون
  static const columnPhoneNumber = 'phone_number';
  static const columnJob = 'job';
  static const columnCode = 'code';
  static const columnAddress = 'address';
  static const columnCardType = 'card_type';


  // تعريف متغيرات الفواتير
  static const columnCustomerId = 'customer_id';
  static const columnDocumentId = 'document_id';
  static const columnProductId = 'product_id';
  static const columnPromoterId = 'promoter_id';
  static const columnMerchantId = 'merchant_id';
  static const columnPaymentCount = 'payment_count';
  static const columnFirstPayment = 'first_payment';
  static const columnInvoiceDate = 'invoice_date';
  static const columnFirstInstallmentDate = 'first_installment_date';
  static const columnIsCash = 'is_cash';
  static const columnIsUsd = 'is_usd';
  static const columnIsQuantity = 'quantity';

  static const columnAgreement = 'agreement';
  static const invoiceId = 'invoice_id';
  static const documentId = 'document_id';



  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }
  Future<bool> databaseExists(String path) async {
    return await databaseFactory.databaseExists(path);
  }

  Future<Database> _initDatabase() async {
    final String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute('''
    CREATE TABLE $productsTable (
      $columnId INTEGER PRIMARY KEY,
      $columnName TEXT NOT NULL,
      $columnPurchasePrice REAL NOT NULL,
      $columnSellingPrice REAL NOT NULL,
      $columnBarcode TEXT NOT NULL,
      $columnQuantityIn INTEGER NOT NULL,
      $columnQuantityOut INTEGER NOT NULL,
      $columnInitialBalance REAL NOT NULL,
      CONSTRAINT unique_name UNIQUE ($columnName)
    )
    ''');
    await db.execute('''
    CREATE TABLE $tableCustomers (
      $columnId INTEGER PRIMARY KEY,
      $columnName TEXT NOT NULL,
      $columnPhoneNumber TEXT NOT NULL,
      $columnJob TEXT NOT NULL,
      $columnCode TEXT NOT NULL,
      $columnAddress TEXT NOT NULL,
      $columnCardType TEXT NOT NULL,
      CONSTRAINT unique_name UNIQUE ($columnName)
    )
    ''');
    await db.execute('''
    CREATE TABLE $tablePromoter (
      $columnId INTEGER PRIMARY KEY,
      $columnName TEXT NOT NULL,
      CONSTRAINT unique_name UNIQUE ($columnName)
    )
    ''');
    await db.execute('''
    CREATE TABLE $tableMerchant (
      $columnId INTEGER PRIMARY KEY,
      $columnName TEXT NOT NULL,
      CONSTRAINT unique_name UNIQUE ($columnName)
    )
    ''');
    await db.execute('''
    CREATE TABLE $tableDocuments (
      $columnId INTEGER PRIMARY KEY,
      $columnName TEXT NOT NULL UNIQUE
    )
  ''');

    await db.execute('''
    CREATE TABLE $tableInvoices (
        $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
        $columnCustomerId INTEGER NOT NULL,
        $columnDocumentId INTEGER NOT NULL,
        $columnProductId INTEGER NOT NULL,
        $columnPromoterId INTEGER,
        $columnMerchantId INTEGER,
        $columnPaymentCount INTEGER NOT NULL,
        $columnFirstPayment REAL NOT NULL,
        $columnInvoiceDate TEXT NOT NULL,
        $columnFirstInstallmentDate TEXT,
        $columnIsCash INTEGER NOT NULL,
        $columnIsQuantity REAL NOT NULL,
        $columnIsUsd INTEGER NOT NULL,
        FOREIGN KEY ($columnCustomerId) REFERENCES customers($columnId),
        FOREIGN KEY ($columnDocumentId) REFERENCES documents($columnId),
        FOREIGN KEY ($columnProductId) REFERENCES products($columnId),
        FOREIGN KEY ($columnPromoterId) REFERENCES promoters($columnId),
        FOREIGN KEY ($columnMerchantId) REFERENCES merchants($columnId)
      )
    ''');

    await db.execute('''
          CREATE TABLE $tableInvoiceDocument (
            $columnId INTEGER PRIMARY KEY,
            $invoiceId INTEGER NOT NULL,
            $documentId INTEGER NOT NULL,
            $columnAgreement TEXT NOT NULL,
            FOREIGN KEY ($invoiceId) REFERENCES invoices($columnId),
            FOREIGN KEY ($documentId) REFERENCES documents($columnId)
          )
        ''');



    // Insert default data
    await db.rawInsert('INSERT INTO $tablePromoter ($columnName) VALUES ("بلا")');
    await db.rawInsert('INSERT INTO $tableMerchant ($columnName) VALUES ("بلا")');

    await insertTestingData(db);

  }

  // delete the entire database
  Future deleteMyDatabase() async {
    String path = await getDatabasesPath();
    String fullPath = join(path, _databaseName);
    await deleteDatabase(fullPath);
    _database = null;
  }


  Future insertTestingData(Database db)async{
    // this data for testing only
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("بطاقة الهوية الوطنية")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("جواز السفر")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("رخصة القيادة")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("بطاقة الإقامة")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("بطاقة الضمان الاجتماعي")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("بطاقة الائتمان")');
    await db.execute('INSERT INTO $tableDocuments ($columnName) VALUES ("شهادة الميلاد")');
    await db.execute('INSERT INTO $tableCustomers ($columnName, $columnPhoneNumber, $columnJob, $columnCode, $columnAddress, $columnCardType) VALUES("أحمد محمد علي", "0123456789", "طبيب", "1234", "القاهرة", "بطاقة ائتمان")');
    await db.execute('INSERT INTO $tableCustomers ($columnName, $columnPhoneNumber, $columnJob, $columnCode, $columnAddress, $columnCardType) VALUES("محمد أحمد علي", "0111111111", "مهندس", "2345", "الجيزة", "بطاقة ائتمان")');
    await db.execute('INSERT INTO $tableCustomers ($columnName, $columnPhoneNumber, $columnJob, $columnCode, $columnAddress, $columnCardType) VALUES("علي محمد أحمد", "0100000000", "مدرس", "3456", "الإسكندرية", "بطاقة ائتمان")');
    await db.execute('''
  INSERT INTO $productsTable ($columnName, $columnPurchasePrice, $columnSellingPrice, $columnBarcode, $columnQuantityIn, $columnQuantityOut, $columnInitialBalance) VALUES
  ("تلفاز سامسونج 55 بوصة", 5000, 7000, "1234567890123", 10, 0, 5000 * (1 + (20/100)))
''');
    await db.execute('''
  INSERT INTO $productsTable ($columnName, $columnPurchasePrice, $columnSellingPrice, $columnBarcode, $columnQuantityIn, $columnQuantityOut, $columnInitialBalance) VALUES
  ("براد توشيبا 12 قدم", 3000, 4500, "2345678901234", 20, 0, 3000 * (1 + (30/100)))
''');
    await db.execute('''
  INSERT INTO $productsTable ($columnName, $columnPurchasePrice, $columnSellingPrice, $columnBarcode, $columnQuantityIn, $columnQuantityOut, $columnInitialBalance) VALUES
  ("لابتوب ايسر اسباير 5", 6000, 9000, "3456789012345", 15, 0, 6000 * (1 + (40/100)))
''');
    await db.execute('''
  INSERT INTO $productsTable ($columnName, $columnPurchasePrice, $columnSellingPrice, $columnBarcode, $columnQuantityIn, $columnQuantityOut, $columnInitialBalance) VALUES
  ("موبايل شاومي نوت 10 برو", 3500, 5500, "4567890123456", 25, 0, 3500 * (1 + (50/100)))
''');
    await db.execute('''
  INSERT INTO $productsTable ($columnName, $columnPurchasePrice, $columnSellingPrice, $columnBarcode, $columnQuantityIn, $columnQuantityOut, $columnInitialBalance) VALUES
  ("جهاز كمبيوتر مكتبي اتش بي", 8000, 12000, "5678901234567", 5, 0, 8000 * (1 + (25/100)))
''');

    await db.rawInsert('INSERT INTO $tableMerchant ($columnName) VALUES ("محمد علي إبراهيم")');
    await db.rawInsert('INSERT INTO $tableMerchant ($columnName) VALUES ("فاطمة محمد حسين")');
    await db.rawInsert('INSERT INTO $tableMerchant ($columnName) VALUES ("يوسف عبد الرحمن علي")');
    await db.rawInsert('INSERT INTO $tablePromoter ($columnName) VALUES ("سارة أحمد إسماعيل")');
    await db.rawInsert('INSERT INTO $tablePromoter ($columnName) VALUES ("علي محمد حسن")');
    await db.rawInsert('INSERT INTO $tablePromoter ($columnName) VALUES ("نورا مصطفى يوسف")');

  }

}