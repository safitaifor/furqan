
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/model/document.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:sqflite/sqflite.dart';

class DocumentOperations {
  var database = DatabaseHelper.instance.database;

  // insert a document into the table
  Future<int> insertDocument(Document document) async {
    try {
      Database db = await database;
      return await db.insert(DatabaseHelper.tableDocuments, document.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        // يوجد قيمة مكررة في الحقل الذي يحتوي على الفهرس الفريد
        if (kDebugMode) {
          showToast(text: 'يوجد مستند بنفس الاسم', color: Colors.redAccent);
          print('خطأ: قيمة مكررة في حقل الاسم');
        }
      } else {
        // خطأ آخر
        if (kDebugMode) {
          print('خطأ: ${e.toString()}');
        }
      }
      return -1;
    }
  }

  // update a document in the table
  Future<int> updateDocument(Document document) async {
    Database db = await database;
    return await db.update(DatabaseHelper.tableDocuments, document.toMap(),
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [document.id!]);
  }

  // delete a document from the table
  Future<int> deleteDocument(int id) async {
    if (id == 1) {
      return 1;
    }
    Database db = await database;
    return await db.delete(DatabaseHelper.tableDocuments, where: '${DatabaseHelper.columnId} = ?', whereArgs: [id]);
  }

  // get a document from the table by id
  Future<Document?> getDocumentById(int id) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableDocuments,
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [id], limit: 1);
    if (maps.isNotEmpty) {
      return Document.fromMap(maps.first);
    } else {
      return null;
    }
  }

  // get all documents from the table
  Future<List<Document>> getAllDocuments() async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableDocuments);
    return List.generate(maps.length, (i) {
      return Document.fromMap(maps[i]);
    });
  }
}