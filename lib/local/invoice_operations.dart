import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import '../local/sql_database.dart';
import '../model/invoice.dart';
import '../widgets/show_toast.dart';

class InvoicesOperations {
  var database = DatabaseHelper.instance.database;

  // insert an invoice into the table
  Future<int> insertInvoice(Invoice invoice) async {
    try {
      Database db = await database;
      final response = await db.insert(DatabaseHelper.tableInvoices, invoice.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
      return response;
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        // يوجد قيمة مكررة في الحقل الذي يحتوي على الفهرس الفريد
        showToast(text: 'عذراً يوجد فاتورة بنفس الرقم', color: Colors.redAccent);
      } else {
        // خطأ آخر
        if (kDebugMode) {
          print('خطأ: ${e.toString()}');
        }
      }
      return -1;
    }
  }

  // update an invoice in the table
  Future<int> updateInvoice(Invoice invoice) async {
    Database db = await database;
    return await db.update(DatabaseHelper.tableInvoices, invoice.toMap(),
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [invoice.id!]);
  }

  // delete an invoice from the table
  Future<int> deleteInvoice(int id) async {
    Database db = await database;
    return await db.delete(DatabaseHelper.tableInvoices, where: '${DatabaseHelper.columnId} = ?', whereArgs: [id]);
  }

  // get an invoice from the table by id
  Future<Invoice?> getInvoiceById(int id) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableInvoices,
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [id], limit: 1);
    if (maps.isNotEmpty) {
      return Invoice.fromMap(maps.first);
    } else {
      return null;
    }
  }

  // get all invoices from the table
  Future<List<Invoice>> getAllInvoices() async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.tableInvoices);
    List<Invoice> invoices = await Future.wait(maps.map((map) => Invoice.fromMap(map)));
    return invoices;
  }
}