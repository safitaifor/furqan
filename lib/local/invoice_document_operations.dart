

import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/model/document.dart';
import 'package:furqan_gallery/model/invoice_document.dart';

class InvoiceDocumentOperations {
  var database = DatabaseHelper.instance.database;

  // إضافة وثيقة لفاتورة
  Future<int> addInvoiceDocument(InvoiceDocument invoiceDocument) async {
    final db = await database;
    return await db.insert(DatabaseHelper.tableInvoiceDocument, invoiceDocument.toMap());
  }

  // تعديل وثيقة لفاتورة
  Future<int> updateInvoiceDocument(
      InvoiceDocument invoiceDocument) async {
    final db = await database;
    return await db.update(
      DatabaseHelper.tableInvoiceDocument,
      invoiceDocument.toMap(),
      where: '${DatabaseHelper.columnId} = ?',
      whereArgs: [invoiceDocument.id],
    );
  }

  // حذف وثيقة من فاتورة
  Future<int> deleteInvoiceDocument(int id) async {
    final db = await database;
    return await db.delete(
      DatabaseHelper.tableInvoiceDocument,
      where: '${DatabaseHelper.columnId} = ?',
      whereArgs: [id],
    );
  }

  // جلب وثيقة بالمعرف
  Future<InvoiceDocument?> getInvoiceDocumentById(int id) async {
    final db = await database;
    final results = await db.query(
      DatabaseHelper.tableInvoiceDocument,
      where: '${DatabaseHelper.columnId} = ?',
      whereArgs: [id],
      limit: 1,
    );
    if (results.isNotEmpty) {
      return InvoiceDocument.fromMap(results.first);
    } else {
      return null;
    }
  }

  // جلب جميع الوثائق لفاتورة
  Future<List<InvoiceDocument>> getAllInvoiceDocuments() async {
    final db = await database;
    final results = await db.query(DatabaseHelper.tableInvoiceDocument,);
    return List.generate(results.length, (i) {
      return InvoiceDocument.fromMap(results[i]);
    });
  }

  Future<List<Document>> getInvoiceDocumentsByInvoiceId(int invoiceId) async {
    final db = await database;

    var response = await db.rawQuery('''
    SELECT invoice_documents.${DatabaseHelper.columnId}, invoice_documents.${DatabaseHelper.invoiceId}, invoice_documents.${DatabaseHelper.documentId}, documents.${DatabaseHelper.columnName}, invoice_documents.${DatabaseHelper.columnAgreement}
    FROM ${DatabaseHelper.tableInvoiceDocument} AS invoice_documents
    JOIN ${DatabaseHelper.tableDocuments} AS documents
    ON invoice_documents.${DatabaseHelper.documentId} = documents.${DatabaseHelper.columnId}
    WHERE invoice_documents.${DatabaseHelper.invoiceId} = $invoiceId
  ''');

    return response.map<Document>((e) => Document.fromMap(e)).toList();
  }



}