import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/model/person.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:sqflite/sqflite.dart';


class PersonOperations {

  var database = DatabaseHelper.instance.database;

  // insert a PersonOperations into the table
  Future<int> insertPerson(Person person) async {
      try {
        Database db =await database;
        return await db.insert(person.type == PersonType.promoter?DatabaseHelper.tablePromoter:DatabaseHelper.tableMerchant, person.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
      } on DatabaseException catch (e) {
        if (e.isUniqueConstraintError()) {
          // يوجد قيمة مكررة في الحقل الذي يحتوي على الفهرس الفريد
          if (kDebugMode) {
            showToast(text: 'يوجد ${person.type == PersonType.promoter?"مروج":"تاجر"} بنفس الاسم', color: Colors.redAccent);
            print('خطأ: قيمة مكررة في حقل الاسم');
          }
        } else {
          // خطأ آخر
          if (kDebugMode) {
            print('خطأ: ${e.toString()}');
          }
        }
        return -1;
      }

  }

  // update a person in the table
  Future<int> updatePerson(Person person) async {
    Database db = await database;
    return await db.update(person.type == PersonType.promoter?DatabaseHelper.tablePromoter:DatabaseHelper.tableMerchant, person.toMap(),
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [person.id!]);
  }

  // delete a person from the table
  Future<int> deletePerson(int id , PersonType type) async {
    if(id == 1){
      return 1;
    }
    Database db = await database;
    return await db.delete(type == PersonType.promoter?DatabaseHelper.tablePromoter:DatabaseHelper.tableMerchant, where: '${DatabaseHelper.columnId} = ?', whereArgs: [id]);
  }

  // get a person from the table by id
  Future<Person?> getPersonById(int id , PersonType type) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(type == PersonType.promoter?DatabaseHelper.tablePromoter:DatabaseHelper.tableMerchant,
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [id], limit: 1);
    if (maps.isNotEmpty) {
      return Person.fromMap(maps.first,PersonType.merchant);
    } else {
      return null;
    }
  }

  // get all Persons from the table
  Future<List<Person>> getAllPersons(PersonType type) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(type == PersonType.promoter?DatabaseHelper.tablePromoter:DatabaseHelper.tableMerchant);
    return List.generate(maps.length, (i) {
      return Person.fromMap(maps[i],type);
    });
  }
}
