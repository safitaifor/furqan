import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:furqan_gallery/local/sql_database.dart';
import 'package:furqan_gallery/model/invoice_document.dart';
import 'package:furqan_gallery/model/product.dart';
import 'package:furqan_gallery/widgets/show_toast.dart';
import 'package:sqflite/sqflite.dart';


class ProductOperations {

  var database = DatabaseHelper.instance.database;

  // insert a product into the table
  Future<int> insertProduct(Product product) async {
    try {
      Database db =await database;
      final response = await db.insert(DatabaseHelper.productsTable, product.toMap(), conflictAlgorithm: ConflictAlgorithm.fail);
      return response;
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        // يوجد قيمة مكررة في الحقل الذي يحتوي على الفهرس الفريد
        if (kDebugMode) {
          showToast(text: 'عذراً يوجد مادة بنفس الاسم', color: Colors.redAccent);
        }
      } else {
        // خطأ آخر
        if (kDebugMode) {
          print('خطأ: ${e.toString()}');
        }
      }
      return -1;
    }
  }

  // update a product in the table
  Future<int> updateProduct(Product product) async {
    Database db = await database;
    return await db.update(DatabaseHelper.productsTable, product.toMap(),
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [product.id!]);
  }

  // delete a product from the table
  Future<int> deleteProduct(int id) async {
    Database db = await database;
    return await db.delete(DatabaseHelper.productsTable, where: '${DatabaseHelper.columnId} = ?', whereArgs: [id]);
  }

  // get a product from the table by id
  Future<Product?> getProductById(int id) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.productsTable,
        where: '${DatabaseHelper.columnId} = ?', whereArgs: [id], limit: 1);
    if (maps.isNotEmpty) {
      return Product.fromMap(maps.first);
    } else {
      return null;
    }
  }

  // get all products from the table
  Future<List<Product>> getAllProducts() async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.productsTable);
    return List.generate(maps.length, (i) {
      return Product.fromMap(maps[i]);
    });
  }
}
